﻿using System.Reflection;
using DroneObject.Variaveis;
using DroneObject.Entity;
namespace DroneControl
{
    public static class DroneControle
    {
        public static void Main(string[] args)
        {
            var drone = new Drone();
            TelaInicial(drone);
        }
        public static void TelaInicial(Drone drone)
        {
            Console.Clear();

            Console.WriteLine("======= Drone =======");
            Console.WriteLine("1 - Voar");
            Console.WriteLine("0 - Desligar");
            int opcao = int.Parse(Console.ReadLine());

            switch (opcao)
            {
                case 1:
                {
                    Voar(drone);
                }break;
                case 0: System.Environment.Exit(0); break;
                default: TelaInicial(drone); break;
            }
        }
        public static void Voar(Drone drone)
        {
            Console.Clear();
            Console.WriteLine($"O seu drone está voando a {drone.Fly} metros do chão. Deseja voar mais alto?");
            var Parse = new Boleano();
            bool opcao = Parse.SimNao(Console.ReadLine());
            switch (opcao)
            {
                case true:
                {
                    VoarAlto(drone);
                }break;
                case false:
                {
                    Desligar(drone);
                }break;
                default: break;
            }    

        }
        public static bool Desligar(Drone drone)
        {
            var Parse = new Boleano();
            do
            {
                Console.WriteLine("Você deseja desligar?");
                bool opcao1 = Parse.SimNao(Console.ReadLine());     
                drone.Fly -= drone.Fly;
                Console.WriteLine("O seu drone está desligado");
                TelaInicial(drone);
            if (!opcao1)
            {
                VoarAlto(drone);
            } 
            } while (true);
       
        }
        public static bool VoarAlto(Drone drone)
        {
            var Parse = new Boleano();
            drone.Estado = true;
            do
            {
            Console.WriteLine("Quanto mais você quer voar em relação ao chão");
            double high = double.Parse(Console.ReadLine());
            drone.Fly += high;
            Console.WriteLine($"O seu drone está voando a {drone.Fly} metros do chão. Deseja voar mais alto?");
            bool opcao = Parse.SimNao(Console.ReadLine());
            Direcao(drone);     
            if(!opcao)
            {
                Desligar(drone);
                break;
            }
            } while (true);
           return drone.Estado;
        }
        public static void Direcao(Drone drone)
        {
            Console.WriteLine("Em qual direção o drone deve ir?");
            int direction = int.Parse(Console.ReadLine());
            drone.Direcao += direction;

        }
    }
}
