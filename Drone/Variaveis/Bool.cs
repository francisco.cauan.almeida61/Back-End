namespace DroneObject.Variaveis
{
    public class Boleano
    {
    public bool SimNao(string input)
    {
        if (input.Equals("sim", StringComparison.OrdinalIgnoreCase))
        {
            return true;
        }
        else if (input.Equals("não", StringComparison.OrdinalIgnoreCase) || input.Equals("nao", StringComparison.OrdinalIgnoreCase))
        {
            return false;
        }
        else
        {
            throw new FormatException("Entrada inválida. Por favor, insira 'sim' ou 'não'.");
        }
    }
  }
}