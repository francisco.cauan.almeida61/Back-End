using System.Text;

namespace DroneObject.Entity
{
    public class Drone
    {
        public Drone(double fly = 0.5 , int direcao = 0, double velocidade = 0.0 , bool estado = false)
        {
            Fly = fly;
            Direcao = direcao;
            Velocidade = velocidade;
            Estado = estado;
        }
        public double Fly { get; set; } = 0.5;
        public int Direcao { get; set; } = 0;
        public double Velocidade { get; set; } = 0.0;
        public bool Estado { get; set; } = false;
       
    }
}