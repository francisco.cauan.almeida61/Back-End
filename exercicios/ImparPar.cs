using System;
using System.ComponentModel.Design;

namespace ImparPar
{
    class ImparPar
    {
        static void MainImparPar(string[] args)
        {
            Menu();
        }
        public static void Menu()
        {
            Console.Clear();
            Console.WriteLine("Olá! Informe um número inteiro:");
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine(" ");
            Calculo(number);
        }
        public static void Calculo(int number)
        {
           
            Console.Clear();
            if (number % 2 == 0)
            {
                Console.WriteLine("O número é par");
            }else{
                Console.WriteLine("O número é impar");
            }

        }
    }
}