using System;

namespace MediaPonderada
{
    class MediaPonderada
    {
        static void MainPon(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Informe a primeira nota:");
            double n1 = double.Parse(Console.ReadLine());
            Console.WriteLine("Informe o peso da primeira nota:");
            int p1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Informe a segunda nota:");
            double n2 = double.Parse(Console.ReadLine());
            Console.WriteLine("Informe o peso da segunda nota:");
            int p2 = int.Parse(Console.ReadLine());
            var peso1 = n1 * p1;
            var peso2 = n2 * p2;
            var result = peso1 / peso2;
            Console.WriteLine($"A média ponderada de {n1} com peso {p1} e {n2} com peso {p2} é {result}");
        }
    }
}