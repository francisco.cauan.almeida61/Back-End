using System;
using System.Text;

namespace ManiString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Informe um nome:");
            string nome = Console.ReadLine();
             //Removendo os espaços laterais
            Console.WriteLine(nome.Trim());
             //Removendo os espaços entre os nomes
            Console.WriteLine(nome.Replace(" ", ""));
             //passe todas as letras para minusculo
            Console.WriteLine(nome.ToLower());
            //passe todas as letras para maiusculo
            Console.WriteLine(nome.ToUpper());
             //adicione uma nova palavra ao nome
            Console.WriteLine(nome.Insert(nome.Length, "almeida"));
             //remova uma palavra do nome
            Console.WriteLine(nome.Replace("almeida", "Francisco"));
            //informe a quantidade de letras no nome
            Console.WriteLine(nome.Replace(" ", "").Length);
            //pesquise uma palavra dentro do nome
            Console.WriteLine(nome.Contains("cauan"));
            // var nome1 = new StringBuilder();
            // nome1.Append("Almeida");
            // nome1.ToString();
            // Console.WriteLine(nome1);

        }
    }
}