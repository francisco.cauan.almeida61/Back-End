using System;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace Data
{
    class Datas
    {
        static void MainDatas(string[] args)
    {
        Console.Clear();
        //var data = new DateTime();
        //Console.WriteLine(data); //Construtor
        var data = DateTime.Now;// Data atual
        //var data = new DateTime(2020, 10, 12, 8, 23, 14); 
        // Console.WriteLine(data);
        // Console.WriteLine(data.Year);
        // Console.WriteLine(data.Minute);
        // Console.WriteLine(data.Day);
        // Console.WriteLine(data.DayOfWeek);
        // Console.WriteLine(data.DayOfYear);
        // Console.WriteLine(data.Second);
        //var data = DateTime.Now;
        //var formatada = String.Format("{0:dd/MM/yyyy hh:mm:ss z}", data);
        var formatada = String.Format("{0:u}", data);
        Console.WriteLine(formatada);
        //Console.WriteLine(data.AddDays(12)); Adiciona 12 dias ao dia atual
        //Console.WriteLine(data.AddHours(5)); Adiciona 5 horas a hora atual
        //Console.WriteLine(data.AddYears(3)); Adiciona 3 anos ao ano atual

        // if (data == DateTime.Now.Date){
        //     Console.WriteLine("É igual"); comparação de datas
        // }
        // Console.WriteLine(data);

        // var pt = new CultureInfo("pt-BR");
        // Console.WriteLine(DateTime.Now.ToString("D", pt));

        // var utcDate = DateTime.UtcNow;
        // Console.WriteLine(DateTime.Now);
        // Console.WriteLine(utcDate);

        // Console.WriteLine(utcDate.ToLocalTime());

        var timeSpan = new TimeSpan();
        Console.WriteLine(timeSpan);
        var timeDiaHoraMinutoSegundo = new TimeSpan(3, 5, 50, 10);
        Console.WriteLine(timeDiaHoraMinutoSegundo);


    }
    }
   
}