using System;

namespace Media
{
    class Media
    {
        static void MainMEdia(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Informe a primeira nota:");
            double num1 = double.Parse(Console.ReadLine());
            Console.WriteLine(" ");
            Console.WriteLine("Informe a segunda nota:");
            double num2 = double.Parse(Console.ReadLine());
            var result = (num1 + num2) / 2;
            Console.WriteLine($"A média de {num1} e {num2} é {result}");
        }
    }
}