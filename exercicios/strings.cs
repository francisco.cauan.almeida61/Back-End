using System;
using System.Text;

namespace StringMan
{
    class StringMain
    {
        static void MainString(string[] args)
        {
            var texto = "  Este texto é um teste  ";
            Console.WriteLine(texto.Replace("Este", "isto"));//Substitui

            var divisao = texto.Split(" ");
            Console.WriteLine(divisao[0]);
            var result = texto.Substring(5, 5);
            Console.WriteLine(result);
            Console.WriteLine(texto.Trim());//Limpar começo e fim
            
        }
        static void MainOI(string[] args)
        {
            var texto = new StringBuilder();
            texto.Append("Este");
            texto.Append(" Texto");
            texto.ToString();

            Console.WriteLine(texto);
        }
    }
}