using System;

namespace Arrays
{
    class Arrays
    {
        static void MainArays(string[] args)
        {
            Console.Clear();
            var meuArray = new int[5] {1,2,3,4,5};
            meuArray[0] = 12;
            Console.WriteLine(meuArray[0]);
            Console.WriteLine(meuArray[1]);
            Console.WriteLine(meuArray[2]);
            Console.WriteLine(meuArray.Length);

            for(var index = 0; index <= meuArray.Length; index++)
            {
                Console.WriteLine(meuArray[index]);
            }

            foreach (var item in meuArray)
            {
                
            }

            //Console.WriteLine(meuArray[0]);
        }
    }
}