using System;

namespace MediaNvalor
{
    class MediaNvalor
    {
        static void MainMediaN(string[] args)
        {
            var num = 0;
            var cont = 0;
            var total = 0;
            Console.Clear();
            do
            {
                Console.WriteLine("Informe um número(Aperte 0 para sair!)");
                num = int.Parse(Console.ReadLine());

                if (num == 0){
                    Console.WriteLine($"A média dos valores é {total/cont}");
                }
                
                total += num;
                cont++;
            } while (num != 0);
        }
    }
}