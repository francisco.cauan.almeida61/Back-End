using System;

namespace StringComp
{
    class StringComp
    {
        static void MainComp(string[] args)
        {
            var texto = "Este Testando";
            var teste = 33;
            Console.WriteLine(teste.Equals(33));
            Console.WriteLine(texto.CompareTo("Testando"));
            Console.WriteLine(texto.Contains("Testa"));
            Console.WriteLine(texto.Contains("Testa", StringComparison.OrdinalIgnoreCase));
            Console.WriteLine(texto.StartsWith("Este"));
            Console.WriteLine(texto.StartsWith("este"));
            Console.WriteLine(texto.EndsWith("Testando"));
            Console.WriteLine(texto.Equals("ste Testando"));
        }
    }
}