using System;

namespace Exceptions
{
    class Exceptions
    {
        static void MainEX(string[] args)
        {
            var arr = new int[3];
            try
            {
                // for (var index = 0; index < 10; index++)
                //  {
                //     // IndexOutOfRangeException
                //      Console.WriteLine(arr[index]);
                //  }
                Cadastrar("fsdfdsf");
            }
            catch (IndexOutOfRangeException ex)
            {
                 Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Não encontrei o indice");//Exception em especifico
            }
                catch (ArgumentNullException ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                Console.WriteLine("Falha ao cadastrar o texto");//Exception em especifico
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                Console.WriteLine(ex.Message);
                Console.WriteLine("ops!");// Exceptions em geral, tirando a de cima
            }
            finally
            {
                    Console.WriteLine("fim");
            }
          
                 
        }
        private static void Cadastrar(string texto) {
            if (string.IsNullOrEmpty(texto))
                //throw new Exception("O texto não pode ser nulo ou vazio!");
                throw new ArgumentNullException("O texto não pode ser nulo ou vazio!");
        
    }
    }
}