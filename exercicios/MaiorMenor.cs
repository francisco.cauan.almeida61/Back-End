using System;
using System.Threading.Tasks.Dataflow;

namespace MaiorMenor
{
    class MaiorMenor
    {
        static void Mainoi(string[] args)
        {
            Menu();
        }
        public static void Menu()
        {
            Console.Clear();
            Console.WriteLine("Informe o primeiro valor:");
            double v1 = double.Parse(Console.ReadLine());
            Console.WriteLine(" ");
            Console.WriteLine("Informe o segundo valor:");
            double v2 = double.Parse(Console.ReadLine());
            Console.WriteLine(" ");
            Console.WriteLine("Informe o terceiro valor:");
            double v3 = double.Parse(Console.ReadLine());
            Console.WriteLine(" ");
            Calculo(v1, v2, v3);
        }

        public static void Calculo(double v1, double v2, double v3)
        {
            Console.Clear();
            double maiorNum = 0;
            double menorNum = 0;
            if (v1 > v2 && v1 > v3 )
            {
                maiorNum = v1;
            } else if (v2 > v1 && v2 > v3)
            {
               maiorNum = v2;
            } else {
               maiorNum = v3;
            }
            if (v1 < v2 && v1 <  v3 )
            {
                menorNum = v1;
            } else if (v2 < v1 && v2 < v3)
            {
                menorNum = v2;
            } else {
                menorNum = v3;
            }

            Console.WriteLine($"O maior valor entre {v1}, {v2} e {v3} é: {maiorNum}");
            Console.WriteLine($"O menor valor entre {v1}, {v2} e {v3} é: {menorNum}");

        }
    }
 }
