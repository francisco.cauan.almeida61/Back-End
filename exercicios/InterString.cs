using System;

namespace InterStrings
{
    class Program
    {
        static void MainInterString(string[] args)
        {
            var price = 10.2;
            //var texto = "O preço do produto é " + price + " em promoção!";
            //var texto = string.Format("O preço do produto é {1} na promoção {0}  ", price, true);
            var texto = $"O preço do produto é {price} apenas na promoção";
            Console.WriteLine(texto);
        } 
    }
}