using System;

namespace Guide
{
    class Program
    {
       static void MainGuide(string[] args)
       {
            var id = Guid.NewGuid();//Gera um indentificador único cada vez que o código é executado
            id.ToString(); //Transforma em string
            id.ToString().Substring(0, 8); //Transforma em string e corta em 8 caracteres
            Console.WriteLine(id);
            id = new Guid("adf907c9");
            Console.WriteLine(id);
            Console.WriteLine( id.ToString().Substring(0, 8));
       } 
    }
}