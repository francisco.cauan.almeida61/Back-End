﻿using System;
using Balta.ContentContext;
using Balta.SubscriptionContext;

namespace Balta
{
    class Program
    {
        

        static void Main(string[] args)
        {
           var articles = new List<Article>();
           articles.Add(new Article ("Artigo Gay", "cuazedo.com.br"));
           articles.Add(new Article ("Artigo Gay2", "cuazedo.com.br"));

           foreach (var article in articles)
           {
                Console.WriteLine(article.Title);
                Console.WriteLine(article.Id);
                Console.WriteLine(article.Url);
           }
           var courses = new List<Course>(); 
           var courseOOP = new Course("Fundamentos OOP", "Fundamentos--oop");
           var courseCsharp = new Course("Fundamentos C#", "Fundamentos--c#");
           var courseAspNet = new Course("Fundamentos ASP.NET", "Fundamentos--ASP.NET");
           courses.Add(courseOOP);
           courses.Add(courseCsharp);
           courses.Add(courseAspNet);

          

           var careers = new List<Career>();
           var careerDotnet = new Career("Especialista .NET", "especialista-dotnet");
           var careerItem = new CareerItem(1, "Comece por aqui", "", courseCsharp);
           var careerItem2 = new CareerItem(2, "aprenda .NET", "", courseAspNet);
           var careerItem3 = new CareerItem(3, "aprenda OOP", "", null);
           careerDotnet.Items.Add(careerItem2);
           careerDotnet.Items.Add(careerItem);
           careerDotnet.Items.Add(careerItem3);
           careers.Add(careerDotnet);

           foreach (var career in careers)
           {
                Console.WriteLine(career.Title);
                foreach (var item in career.Items.OrderBy(x => x.Ordem))
                {
                    Console.WriteLine($"{item.Ordem } - {item.Title}");
                    Console.WriteLine(item.Course?.Title);
                    Console.WriteLine(item.Course?.Level);

                    foreach (var notification in item.Notifications)
                    {
                        Console.WriteLine($"{notification.Property} - {notification.Message}");
                    }
                }
                var paypalSubscription = new PayPalSubscription();
                var student = new Student();
                student.CreateSubscription(paypalSubscription);
           }

        }
    }    
}
