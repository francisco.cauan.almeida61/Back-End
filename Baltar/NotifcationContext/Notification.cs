namespace Balta.NotificationContext
{
    public sealed class Notifcation 
    {
        public Notifcation()
        {

        }

        public Notifcation(string property, string message)
        {
            Property = property;
            Message = message;
        }

        public string Property { get; set; }
        public string Message { get; set; }
    }
}