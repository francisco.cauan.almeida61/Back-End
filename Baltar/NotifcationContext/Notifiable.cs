using System.Collections.Generic;
namespace Balta.NotificationContext
{
    public abstract class Notifiable
    {
        public List<Notifcation> Notifications { get; set; }

        public Notifiable ()
        {
            Notifications = new List<Notifcation>();
        }

        public void AddNotification(Notifcation notifcation)
        {
            Notifications.Add(notifcation);
        }
        public void AddNotifications(IEnumerable<Notifcation> notifcations)
        {
            Notifications.AddRange(notifcations);
        }

        public bool IsInvalid => Notifications.Any();
    }
}