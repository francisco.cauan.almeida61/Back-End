using System;

class Lists
{
    static void Main(string[] args)
    {
       // IEnumerable<Payment> payments = new List<Payment>();
       var payments = new List<Payment>();
       payments.Add(new Payment(1));
       payments.Add(new Payment(2));
       payments.Add(new Payment(3));
       foreach (var item in payments)
       {
            Console.WriteLine(item.Id);
       }
       var payment = payments.First(x => x.Id == 3);
       Console.WriteLine(payment.Id);


    }
    public  class Payment
    {
        public int Id { get; set; }
        public Payment(int id)
        {
            Id = id;
        }
    }
}