using System;
using System.Data.Common;

namespace Static
{
    class Static
    {
        public static void MainStatic(string[] args)
        {

        } 
        public static class Pagamento

        {
           public static DateTime Vencimento { get; set; }
        }
        public static class Settings
        {
            public static string API_URL { get; set; }
        }
        public sealed class PagamentoBoleto
        {

        }

        public partial class Payment
        {
            
        }
        public interface IPagamento
        {
           public int MyProperty { get; set; }
        }
        public abstract class Pagamento2
        {

        }
    }
} 

//Uma classe partial é uma classe que pode ser separada em outros arquivos. Por exemplo, partial Payment pode ser implementado em dois arquivos diferentes
//Uma classe sealed não pode ser herdada
//Uma classe abstrata que não pode ser instanciada e que deve conter as caracteristicas mais importantes do seu código.
//Uma classe estática não pode ser instanciada e não pode ser mudada
//Interface é um tipo de classe que mais se asemelha a um contrato. Na interface é definido o que precisa ser feito. As outras classes pode herdar de uma interface, se, implementarem os mesmos métodos.
//Conceito de upcast é quando uma classe filha tem os mesmos métodos herdados da classe pai, e os métodos da classe filho mudam os atributos da classe pai.
//Conceito de downcast é quando acontece uma conversão explicita entre duas classes Pai e Filho. pessoaFisica= (pessoaFisica)pessoa;
//Podemos escrever um método booleano para comparar objetos e classes. Que é o equals
// public bool Equals(Pessoa pessoa)
// {
//     return Id == pessoa.Id;
// }
//  var pessoaA = new Pessoa(1, "Cuazedo");
//  var pessoaB = new Pessoa(1, "viado");
//  Console.WriteLine(pessoaA == pessoaB); return true
//O método delegate delega a realização do método em outro método.