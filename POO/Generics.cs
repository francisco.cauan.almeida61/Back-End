

    using System;

class Generics
{
    public static void Main(string[] args)
    {
        var person = new Person();
        var payment = new Payment();
        var context = new DataContext<Person, Payment>();
        context.Save(person);
        context.Save(payment);
    }
    public class DataContext<P, PA> 
    where P : Person
    where PA : Payment
    {
        public void Save(P entity)
        {}
        public void Save(PA entity)
        {}

    }
    public class Person{}
    public class Payment{}
}
