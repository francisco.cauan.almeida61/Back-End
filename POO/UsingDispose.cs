using System;

namespace UsingDispose
{
    class UsingDispose
    {
        public static void MainUsingDispose(string[] args)
        {
            using (var pagamento = new Pagamento())
            {
                Console.WriteLine("Processando pagamento");
            }
            
        }
        public class Pagamento : IDisposable
        {
            public Pagamento()
            {
                Console.WriteLine("Iniciando o pagamento");
            }

            public void Dispose()
            {
                Console.WriteLine("Finalizando o pagamento");
            }
        }

    }
}