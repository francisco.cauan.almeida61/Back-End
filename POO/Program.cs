﻿using System;
using System.IO.Pipes;
using System.Reflection.Emit;
using System.Runtime.InteropServices.Marshalling;

namespace Pagament
{
    class Program
    {
        public static void MainClasses(string[] args)
        {
        
           
        }
    }
    // private(visivel somente para a classe),  
    // protected (visivel para os filhos da classe), 
    // internal(visivel no mesmo namespace) e 
    // public(visivel para todos)
    class Pagamento
    {
        // Propiedades
        private DateTime Vencimento;
        protected DateTime Vencimento1;
        //Métodos
        public virtual void Pagar(string numero){
       
        }
        public void Pagar(string numero, DateTime vencimento){

        }
        // Agrupar todas as propiedades, eventos e métodos em um classe, se chama encapsulamento.
    }
    class PagamentoBoleto : Pagamento
    {
        void  Test()
        {
            base.Vencimento1 = new DateTime(0);//base. : interaji com itens da classe pai
        }
        public DateTime Vencimento;

        public string NumeroBoleto;

        public override void Pagar(string numero) {

        }
    }

    class PagamentoPix : Pagamento
    {
        public string Numero;
        //propiedades:
        private DateTime _dataPagamento;     
        public DateTime DataPagamento
        {
            get { 
                Console.WriteLine("lendo o valor");
                return _dataPagamento; 
                }
            set {
                Console.WriteLine("Atribuindo valor");
                 _dataPagamento = value; 
            }
        }
        
    }
    
}
//sobrecarga de métodos: Metodos iguais com parametros diferentes
//sobrescrita de métodos: Uma forma de reescrever um método em outro método, você pode chamar o método pai usando base.
