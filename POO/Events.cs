using System;
using System.ComponentModel.Design.Serialization;

class Events
{
    public static void Main(string[] args)
    {
        var room  = new Room(3);
        
        room.RoomSoldOutEvent += OnRoomSoldOut;
        room.ReserveSeat();
        room.ReserveSeat();
        room.ReserveSeat();
       
    }

    private static void OnRoomSoldOut( object source, EventArgs e)
    {
        Console.WriteLine("Sala lotada");
        
    }

   
    public class Room {
        public Room(int seats)
        {
            Seats = seats;
            seatsInUse = 0;
        }
        private int seatsInUse = 0;
        public int Seats { get; set; }
        public void ReserveSeat()
        {
            seatsInUse++;
            if (seatsInUse >= Seats)
            {
                OnRoomSoldOut(EventArgs.Empty);
            }
        }

        public event EventHandler RoomSoldOutEvent;
        protected virtual void OnRoomSoldOut(EventArgs e)
        {
            EventHandler handler = RoomSoldOutEvent;
            handler?.Invoke(this, e);
        }
    }
}