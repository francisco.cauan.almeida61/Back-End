﻿using PaginaLogin.SharedContext;

namespace PaginaDeLogin;

public static class PaginaDeLogin
{
    public static void Main(string[] args)
    {
        var usuarios = new List<Usuario>();
        
        TelaInicial(usuarios);
    }

    public static void TelaInicial(List<Usuario> usuarios)
    {
        Console.Clear();
        
        Console.WriteLine("===== Caixa Eletrônico =====");
        Console.WriteLine("1- Acessar Caixa");
        Console.WriteLine("2- Cadastrar usuário");
        Console.WriteLine("0- Sair");

        int opcao = int.Parse(Console.ReadLine());

        switch (opcao)
        {
            case 1:
            {
                AcessarCaixa(usuarios, Login(usuarios));
            } break;
            
            case 2:
            {
                var usuario = new Usuario("", "");

                usuario = Signin(usuario);
                
                usuarios.Add(usuario);
                
                Console.WriteLine("\nUsuário cadastrado com sucesso!");
                
                Thread.Sleep(3000);
                
                TelaInicial(usuarios);
            } break;
            
            case 0: System.Environment.Exit(0); break;
                
            default: TelaInicial(usuarios); break;
        }
    }

    public static void AcessarCaixa(List<Usuario> usuarios, Usuario usuarioLogado)
    {
       Console.Clear();
        Fazeroperacoes(usuarios, usuarioLogado);
    }

 public static Usuario Signin(Usuario usuario)
    {
        Console.Clear();
        
        Console.WriteLine("===== Signin =====");
        Console.WriteLine("Nome: ");
        usuario.Nome = Console.ReadLine();
        Console.WriteLine("Senha: ");
        usuario.Senha = Console.ReadLine();

        if (usuario.Nome == string.Empty || usuario.Senha == string.Empty)
        {
            Console.WriteLine("\nUm ou mais campos estão incorretos! Por favor corrija-os");
            Thread.Sleep(3000);
            Signin(usuario);
        }

        return usuario;
    }

    public static Usuario Login(List<Usuario> usuarios)
    {
          Console.Clear();
        
        if (usuarios.Count == 0)
        {
            Console.WriteLine("O sistema deve conter ao menos um usuário para funcioar!");
            Thread.Sleep(3000);
            TelaInicial(usuarios);
        }

        Console.WriteLine("===== Login =====");
        Console.WriteLine("Nome: ");
        var nome = Console.ReadLine();
        Console.WriteLine("Senha: ");
        var senha = Console.ReadLine();

        var usuario = usuarios.Find(u => u.Nome == nome && u.Senha == senha);

        if (usuario != null)
        {
            Console.WriteLine($"Bem vindo {usuario.Nome}!");
            Thread.Sleep(3000);
            Login(usuarios);
        }
        Console.WriteLine("\nUm ou mais campos estão incorretos! Por favor corrija-os");
        Thread.Sleep(3000);
        return usuario;
    }

    public static void Fazeroperacoes(List<Usuario> usuarios, Usuario usuarioLogado)
    {
        Console.Clear();
        Console.WriteLine("===== SysCaixa ======");
        Console.WriteLine("1 - Depositar");
        Console.WriteLine("2 - Sacar");
        Console.WriteLine("3 - Verificar saldo");
        Console.WriteLine("0 - Voltar");

        int opcao = int.Parse(Console.ReadLine());
        switch (opcao)
        {
            case 0 : TelaInicial(usuarios); break;
            case 1 :
            {
                Depositar(usuarioLogado);
                Thread.Sleep(3000);
                Fazeroperacoes(usuarios, usuarioLogado);
            } break;
            case 2:
            {
                Sacar(usuarioLogado);
                Thread.Sleep(3000);
                Fazeroperacoes(usuarios, usuarioLogado);
            } break;

            case 3:
            {
                Saldo(usuarioLogado);
                Thread.Sleep(3000);
                Fazeroperacoes(usuarios, usuarioLogado);
            }break;
            default: Fazeroperacoes(usuarios, usuarioLogado); break;
        }
    }
    public static Usuario Depositar(Usuario usuario)
    {
        Console.Clear();
        Console.WriteLine("====== Depositar ======");
        Console.WriteLine("Informe o valor a ser depositado: ");
        decimal valor = decimal.Parse(Console.ReadLine());
        if (valor <= 0 || valor == null)
        {
            Console.WriteLine("\nO valor inserido não pode ser nulo ou negativo! Por favor tente novamente");
            Thread.Sleep(3000); 
            Depositar(usuario);
        }
        usuario.Saldo = usuario.Saldo + valor;
        Console.WriteLine($"\nSaldo atual: {usuario.Saldo}");
        Thread.Sleep(3000);
        return usuario;
    }
    public static Usuario Sacar(Usuario usuario)
    {
        return usuario;
    }
    public static Usuario Saldo(Usuario usuario)
    {
        return usuario;
    }
    
}