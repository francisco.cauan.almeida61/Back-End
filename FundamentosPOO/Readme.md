Criar um sistema de reserva de bancos das salas de um cinema

O Cinema contém 6 salas com 60 cadeiras cada sala.
O Cinema tem horário variados para cada sala.
Cada sala passará um filme diferente a cada duas horas de duração e a cada filme, a sala precisará passar por uma limpeza de 30 min. Por exemplo, sala 1 filme A 13:00 até 15:00, sala 1 filme B, 15:30 até 17:30.
Os horários de funcionamento começam as 13 horas e terminam as 22 horas.
O usuário devera escolher quantas cadeiras deve pegar e deve ser retornado o preço
Uma cadeira custa 4,50 
O usuário, primeiro, deve informar qual filme irá assistir e depois escolher quais horários estão disponiveis para aquele filme
Se a sala estiver lotada naquele horário, o usuário deve ser informado que aquela sala não está disponível.